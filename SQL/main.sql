/* 3.1.1 Create the initial table */
CREATE TABLE main_dataset
(
	t	 	bigint,
--	type 		integer,
	mmsi 		integer,
--	status 		integer,
	lon 		double precision,
	lat 		double precision,
	heading 	integer,
	turn 		double precision,
	speed 		double precision,
	course 		double precision,	
	PRIMARY KEY (t, mmsi, lon, lat)
);

/* ------------------------------------------------------------------ */

/* 3.1.2 Add geometry column */
ALTER TABLE main_dataset ADD COLUMN geom geometry(Point, 4326);
UPDATE main_dataset SET geom = ST_SetSRID(ST_MakePoint(lon, lat), 4326);
/* Delete lat and lon columns */
-- ALTER TABLE first_dataset DROP COLUMN lon;
-- ALTER TABLE first_dataset DROP COLUMN lat;

/* Number of records */
select count(*) from main_dataset;
-- Result: 28125253
/* ------------------------------------------------------------------ */

/* 3.2.1 Create a point somewhere in the middle of Argosaronikos */
select st_setsrid(st_makepoint(23.50000,37.60000), 4326);

/* Create a polygon around that point */
select st_buffer(
		st_transform(
			st_setsrid(
				st_makepoint(23.4,37.75)
				, 4326), 
			2100), 
		40000);

/* Get ships within a radius */
--select st_transform(md.geom, 2100) as geom
--from (
--		select st_buffer(σt_transform(st_setsrid(σt_makepoint(23.50000,37.60000), 4326), 2100), 70000)
--	) as temp,
--	main_dataset as md
--where st_contains(st_transform(temp.geom, 4326), st_transform(md.geom, 4326));

/* Delete all records that the corresponding points are out of the circle */
DELETE FROM main_dataset
USING (select st_buffer(st_transform(st_setsrid(st_makepoint(23.4,37.75), 4326), 2100), 40000) as geom) as temp
WHERE NOT st_contains(st_transform(temp.geom, 4326), st_transform(main_dataset.geom, 4326));

/* Delete records that correspond on land */
/* Create index */
CREATE INDEX oria_dhwmn_idx 
ON oria_dhmwn_kallikraths 
USING GIST (geom);

/* Remove polygons from oria_dhmwn_kallikraths if not near the circle */
DELETE FROM oria_dhmwn_kallikraths
USING (select st_buffer(st_transform(st_setsrid(st_makepoint(23.4, 37.75), 4326), 2100), 40000) as geom)
	AS temp
WHERE not st_intersects(st_transform(temp.geom, 2100), st_transform(oria_dhmwn_kallikraths.geom, 2100));

/* Delete points that contained in land's geometries */
DELETE FROM main_dataset
USING oria_dhmwn_kallikraths
WHERE st_contains(st_transform(oria_dhmwn_kallikraths.geom, 4326), st_transform(main_dataset.geom, 4326));

/* ------------------------------------------------------------------ */

/* Delete records where speed exceeds 200 knots */
delete from main_dataset
where speed > 200;

/* ------------------------------------------------------------------ */

/* For 3.2.2 */
SELECT  date_part('month', (to_timestamp(t/1000)) at time zone 'GMT-3') AS "Month",
        date_part('day', (to_timestamp(t/1000)) at time zone 'GMT-3') AS "Day",
    COUNT(*) AS "Records"
FROM main_dataset
GROUP BY
    date_part('day', (to_timestamp(t/1000)) at time zone 'GMT-3'),
    date_part('month', (to_timestamp(t/1000)) at time zone 'GMT-3')
ORDER BY
    "Month",
    "Day";

update main_dataset
set speed = 0
where speed is null;

/* ------------------------------------------------------------------ */

/* Create a table with all the unique mmsis and the trajectory of each */
create table trajectories(
	mmsi integer,
	traj geom
);

alter table trajectories add CONSTRAINT uniqueness unique (mmsi, traj);

insert into trajectories(mmsi)
select distinct(main_dataset.mmsi) from main_dataset;

select count(mmsi) from trajectories; 
-- Result: 2239

/* ------------------------------------------------------------------ */

/* 3.3.1 Create index */
CREATE INDEX main_dataset_idx
ON main_dataset
USING GIST (geom);

/* Range queries */
/* e.g. which ships are inside Piraeus port */
/* Create a point somewhere in the middle of the Piraeus port */
select st_setsrid(st_makepoint(23.63000,37.94000), 4326);

/* Create a polygon around that point */
select st_buffer(
		st_transform(
			st_setsrid(
				st_makepoint(23.63000,37.94000)
				, 4326), 
			2100), 
		1500);
		
/* Execute the range query based on the index */
/* Which ships are inside the port of Piraeus,
   between 2018-8-1, 8:00:00 - 20:00:00 */
select mmsi	-- instead, for geometries: st_transform(main_dataset.geom, 2100) as geom
from main_dataset, (
	select st_buffer(st_transform(st_setsrid(st_makepoint(23.63,37.94), 4326), 2100), 1500) as circle
	) as temp
where st_contains(st_transform(temp.circle, 4326), st_transform(main_dataset.geom,4326))
	and t > 1533099600000 and t < 1533142800000;

/* Which ships are inside Argosaronikos but not inside the Piraeus port */
select st_transform(main_dataset.geom, 2100) as geom
from main_dataset, (
	select st_buffer(st_transform(st_setsrid(st_makepoint(23.63,37.94), 4326), 2100), 1500) as circle
	) as small, (
	select st_buffer(st_transform(st_setsrid(st_makepoint(23.4, 37.75), 4326), 2100), 40000) as geom
	) as big
where st_contains(st_transform(big.geom, 4326), st_transform(main_dataset.geom, 4326))
	and not 
	st_contains(st_transform(small.circle, 4326), st_transform(main_dataset.geom,4326));

/* ------------------------------------------------------------------ */

/* 3.3.2 Set trajectories and segment them */
/* Create one big trajectory for each ship */
update trajectories
set traj = temp.geom
from (
		select st_makeline(st_transform(ST_SetSRID(st_makepoint(
			md.lon::REAL, md.lat::REAL, md.t::REAL), 4326),	2100)) 
		as geom, trajectories.mmsi
		from main_dataset as md, trajectories
		where md.mmsi = trajectories.mmsi
		group by trajectories.mmsi
	) as temp
where temp.mmsi = trajectories.mmsi


/* ------------------------------------------------------------------ */

/* Count how many trajectories are valid */
select count(traj) from trajectories where not st_isvalid(traj);

/* Delete records where trajectory is not valid */
delete from trajectories where not st_isvalid(traj);

/* Create index on trajectories table */

/* ------------------------------------------------------------------ */

-- temp

