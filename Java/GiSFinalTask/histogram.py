import os
import sys
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

if os.path.exists("output.csv"):
    df = pd.read_csv("output.csv", header=None)
else:
    print("File not found!")

# print(df.head())

sns.barplot(x=0, y=1 , data=df, orient="v", color="salmon", ci=None)
plt.xlabel("Days")
plt.ylabel("Records")
plt.title("Records per Day")
plt.show()