import java.io.Serializable;

import com.vividsolutions.jts.geom.Geometry;

public class Geoms implements Serializable {

	public Geoms() {
	}
	
	private static final long serialVersionUID = 4473242263432415768L;
	
	private int geomid;
	private Geometry geom;
	private int part_id;
	public int getGeomid() {
		return geomid;
	}
	public void setGeomid(int geomid) {
		this.geomid = geomid;
	}
	public Geometry getGeom() {
		return geom;
	}
	public void setGeom(Geometry geom) {
		this.geom = geom;
	}
	public int getPart_id() {
		return part_id;
	}
	public void setPart_id(int part_id) {
		this.part_id = part_id;
	}
	@Override
	public String toString() {
		return "Geoms [geomid=" + geomid + ", geom=" + geom + ", part_id=" + part_id + "]";
	}
	
}
