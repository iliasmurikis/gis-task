import com.vividsolutions.jts.geom.*;
import org.apache.commons.lang.time.StopWatch;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.datasyslab.geospark.enums.GridType;
import org.datasyslab.geospark.geometryObjects.Circle;
import org.datasyslab.geospark.spatialRDD.SpatialDF;
import org.geotools.geometry.jts.JTS;
import scala.Tuple2;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.NoSuchElementException;

import static java.lang.System.exit;
import static java.lang.System.out;


public class masterClass2 {
    public static double distance(double lat1, double lat2, double lon1,
                                  double lon2, double el1, double el2) {

        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters

        double height = el2 - el1;

        distance = Math.pow(distance, 2) + Math.pow(height, 2);

        return Math.sqrt(distance);
    }

    public static void main(String[] args) throws IOException {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        SparkConf conf = new SparkConf();
        conf.setAppName("JavaSparkPi").setMaster("local[*]");
        JavaSparkContext jsc = new JavaSparkContext(conf);
        Logger.getLogger("org.apache").setLevel(Level.WARN);
        // jsc.setLogLevel("WARN");   // ALL, DEBUG, ERROR, FATAL, INFO, OFF, TRACE, WARN

        String julCSV = "/home/none/IdeaProjects/GiSFinalTask/data/jul2018.csv";
        String augCSV = "/home/none/IdeaProjects/GiSFinalTask/data/aug2018.csv";
        JavaRDD<String> julRDD = jsc.textFile(julCSV, 4);
        JavaRDD<String> augRDD = jsc.textFile(augCSV, 4);
        /* Merge the two RDDs */
        JavaRDD<String> datasetRDD = julRDD.union(augRDD);

        /* Casting lat and lon fields into a new Geometry object */
        JavaRDD<Tuple2<Geometry, Object>> rdd = datasetRDD.map( s -> {
            String arr[] = s.split(";");
            /* Convert lat and lon to SRID point */
            GeometryFactory geometryFactory = new GeometryFactory();
            Point point = geometryFactory.createPoint(
                    new Coordinate(Double.parseDouble(arr[4]), Double.parseDouble(arr[5])));

            return new Tuple2<>(point, s);
        });

        /* Output first 10 records */
        // rdd.take(10).forEach(out::println);

        /* Create a circle for the borders Argosaronikos */
        float radius = 70000;
        GeometryFactory geometryFactory = new GeometryFactory();
        Point argosaronikosCenter = geometryFactory.createPoint(new Coordinate(23.5, 37.6));

        /* Ignore records which are outside the radius */
        JavaRDD<Tuple2<Geometry, Object>> newRDD = rdd.map(s -> {
            Point p = (Point) s._1;
            double x1,x2,y1,y2;
            double dis;
            x1 = p.getX();
            y1 = p.getY();
            x2 = argosaronikosCenter.getX();
            y2 = argosaronikosCenter.getY();

            dis = distance(y1, y2 , x1 ,x2 , 0, 0);

            if(dis < radius){
                return s;
            } else {
                return null;
            }
        });

        /* Remove null values */
        JavaRDD<Tuple2<Geometry, Object>> clearedRDD = newRDD.filter(s -> s != null);

        /* Create index */
        /*String geoSparkPartitioner = "rtree";
        SpatialDF spatialDF = new SpatialDF();
        spatialDF.setRawSpatialRDD(clearedRDD);
        spatialDF.analyze();

        try {
            if (geoSparkPartitioner == "quadtree") {
                spatialDF.spatialPartitioning(GridType.QUADTREE);
            } else if (geoSparkPartitioner == "voronoi") {
                spatialDF.spatialPartitioning(GridType.VORONOI);
            } else if (geoSparkPartitioner == "rtree") {
                spatialDF.spatialPartitioning(GridType.RTREE);
            }else {
                out.println("Unknown partitioner!");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        JavaRDD<?> repartition=spatialDF.getSpatialPartitionedRDD();

        JavaRDD<Geoms> partitionMBB_index = repartition.mapPartitionsWithIndex((index,x)->{
            Iterator<?> it= x;
            try {
                String s = (String) it.next();
                String arr[] = s.split(";");
                Point point = geometryFactory.createPoint(
                        new Coordinate(Double.parseDouble(arr[4]), Double.parseDouble(arr[5])));
                //geom.overlaps(g);
                int srid=point.getSRID();
                Envelope boundary = point.getEnvelopeInternal();

                double minX = boundary.getMinX();
                double minY = boundary.getMinY();
                double maxX = boundary.getMaxX();
                double maxY = boundary.getMaxY();

                while (it.hasNext()) {
                    s = (String) it.next();
                    arr = s.split(";");
                    Point point2 = geometryFactory.createPoint(
                            new Coordinate(Double.parseDouble(arr[4]), Double.parseDouble(arr[5])));

                    boundary = point2.getEnvelopeInternal();

                    if (minX > boundary.getMinX()) {
                        minX = boundary.getMinX();
                    }
                    if (maxX < boundary.getMaxX()) {
                        maxX = boundary.getMaxX();
                    }

                    if (minY > boundary.getMinY()) {
                        minY = boundary.getMinY();
                    }
                    if (maxY < boundary.getMaxY()) {
                        maxY = boundary.getMaxY();
                    }
                }

                Envelope mbb = new Envelope(minX, maxX, minY, maxY);
                Polygon polygon = JTS.toGeometry(mbb);
                polygon.setSRID(srid);

                Geoms g=new Geoms();

                g.setGeom(polygon);
                g.setGeomid(-1);
                g.setPart_id(index);

                ArrayList<Geoms> ret= new ArrayList<Geoms>();

                ret.add(g);

                return ret.iterator();

            } catch (NoSuchElementException e) {
                return new ArrayList<Geoms>().iterator();
            }
        }, false);*/

        /* Create a point in the center of Piraeus port */
        float radius2 = 1500;
        Point portOfPiraeusCenter = geometryFactory.createPoint(new Coordinate(23.63,37.94));

        JavaRDD<Point> finalPointsInsidePortRDD = clearedRDD.map( o -> {
            Point p = (Point) o._1;
            double x1,x2,y1,y2;
            double dis;
            x1 = p.getX();
            y1 = p.getY();
            x2 = portOfPiraeusCenter.getX();
            y2 = portOfPiraeusCenter.getY();

            dis= distance(y1, y2, x1, x2, 0, 0);

            if(dis < radius2){
                return p;
            } else {
                return null;
            }
        });

        /*out.println("\nExporting points which are inside the port: \n");
        try {
            finalPointsInsidePortRDD.coalesce(1).saveAsTextFile("points_inside_port");
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
        out.println("Export completed. \n");*/

        /* Remove null values */
        JavaRDD<Point> finalPointsInsidePortRDD_Cleared = finalPointsInsidePortRDD.filter(s -> s != null);

        /* Count how many points are inside */
        long numberOfPoints = finalPointsInsidePortRDD_Cleared.count();
        out.println("\n");
        out.println("Number of points inside the port: " + numberOfPoints);

        stopWatch.stop();
        out.println("Time is: " + stopWatch.getTime());

        jsc.close();
    }
}