import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import org.apache.commons.lang.time.StopWatch;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.datasyslab.geospark.geometryObjects.Circle;
import scala.Tuple2;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Calendar;
import java.util.Map;
import java.util.TreeMap;


public class masterClass {
    public static double distance(double lat1, double lat2, double lon1,
                                  double lon2, double el1, double el2) {

        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters

        double height = el2 - el1;

        distance = Math.pow(distance, 2) + Math.pow(height, 2);

        return Math.sqrt(distance);
    }

    public static void main(String[] args) throws IOException {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        SparkConf conf = new SparkConf();
        conf.setAppName("JavaSparkPi").setMaster("local[*]");
        JavaSparkContext jsc = new JavaSparkContext(conf);
        Logger.getLogger("org.apache").setLevel(Level.WARN);
        // jsc.setLogLevel("WARN");   // ALL, DEBUG, ERROR, FATAL, INFO, OFF, TRACE, WARN

        String julCSV = "/home/none/IdeaProjects/GiSFinalTask/data/jul2018.csv";
        String augCSV = "/home/none/IdeaProjects/GiSFinalTask/data/aug2018.csv";
        JavaRDD<String> julRDD = jsc.textFile(julCSV, 4);
        JavaRDD<String> augRDD = jsc.textFile(augCSV, 4);
        /* Merge the two RDDs */
        JavaRDD<String> datasetRDD = julRDD.union(augRDD);

        /* Casting lat and lon fields into a new Geometry object */
        JavaPairRDD<Geometry, Object> rdd = datasetRDD.mapToPair( s -> {
            String arr[] = s.split(";");
            /* Convert lat and lon to SRID point */
            GeometryFactory geometryFactory = new GeometryFactory();
            Point point = geometryFactory.createPoint(
                    new Coordinate(Double.parseDouble(arr[4]), Double.parseDouble(arr[5])));

            return new Tuple2<>(point, s);
        });

        /* Output first 10 records */
        // rdd.take(10).forEach(System.out::println);

        /* Create a circle for the borders Argosaronikos */
        Double radius = 70000.0;
        GeometryFactory geometryFactory = new GeometryFactory();
        Point argosaronikosCenter = geometryFactory.createPoint(new Coordinate(23.5, 37.6));

        /* Ignore records which are outside the radius */
        JavaPairRDD<Geometry, Object> newRDD = rdd.mapToPair(s -> {
            Point p = (Point) s._1;
            double x1,x2,y1,y2;
            double dis;
            x1 = p.getX();
            y1 = p.getY();
            x2 = argosaronikosCenter.getX();
            y2 = argosaronikosCenter.getY();
            dis = distance(y1 ,y2 ,x1 ,x2 ,0,0);

            if(dis <= radius){
                return s;
            } else {
                return null;
            }
        });

        JavaPairRDD<Geometry, Object> clearedRDD = newRDD.filter(s -> s != null);
        
        /* Calculate records per day */
        JavaPairRDD<String, String> dailyRecordsRDD = clearedRDD.mapToPair(s ->{
            String foo[] = (s._2.toString()).split(";");
            // timestamp is foo[0]
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(Long.parseLong(foo[0]));
            int month = cal.get(Calendar.MONTH);
            int day;

            if (month == 6){
                day = cal.get(Calendar.DAY_OF_MONTH);
            } else {
                day = cal.get(Calendar.DAY_OF_MONTH) + 30;
            }

            return new Tuple2<>(Integer.toString(day), foo[2]);
        });

        JavaPairRDD<Integer, Integer> convertedToOnes = dailyRecordsRDD.mapToPair(s ->
                new Tuple2<>(Integer.valueOf(s._1),1));

        /* Sum all elements of each row */
        Map<Integer, Long> sum = convertedToOnes.countByKey();

        /* Sort values */
        Map<Integer, Long> sortedSumMap = new TreeMap<>(sum);

        System.out.println("\nExporting sorted map: \n");
        // sortedSumMap.forEach((key, value) -> System.out.println(key + " : " + value));

        String eol = System.getProperty("line.separator");

        try (Writer writer = new FileWriter("output.csv")) {
            for (Map.Entry<Integer, Long> entry : sortedSumMap.entrySet()) {
                writer.append(entry.getKey().toString())
                        .append(',')
                        .append(entry.getValue().toString())
                        .append(eol);
            }
        } catch (IOException ex) {
            ex.printStackTrace(System.err);
        }
        System.out.println("Export completed. \n");

        Process process = Runtime.getRuntime().exec("python3.7 histogram.py");

        stopWatch.stop();
        System.out.println("Time is: " + stopWatch.getTime());
        /* Output:
        *   Time is:
        *       36475 */

        jsc.close();
    }
}