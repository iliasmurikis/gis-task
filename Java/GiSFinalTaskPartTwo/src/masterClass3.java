import org.apache.commons.lang.time.StopWatch;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import org.apache.spark.mllib.clustering.KMeans;
import org.apache.spark.mllib.clustering.KMeansModel;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;
import scala.Tuple2;

import java.io.IOException;
import java.util.Arrays;

import static java.lang.System.out;


public class masterClass3 {
    public static void main(String[] args) throws IOException {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        SparkConf conf = new SparkConf();
        conf.setAppName("JavaSparkPi").setMaster("local[*]");
        JavaSparkContext jsc = new JavaSparkContext(conf);
        Logger.getLogger("org.apache").setLevel(Level.WARN);
        // jsc.setLogLevel("WARN");   // ALL, DEBUG, ERROR, FATAL, INFO, OFF, TRACE, WARN

        String julCSV = "/home/none/IdeaProjects/GiSFinalTask/data/jul2018.csv";
        String augCSV = "/home/none/IdeaProjects/GiSFinalTask/data/aug2018.csv";
        JavaRDD<String> julRDD = jsc.textFile(julCSV, 4);
        JavaRDD<String> augRDD = jsc.textFile(augCSV, 4);
        /* Merge the two RDDs */
        JavaRDD<String> datasetRDD = julRDD.union(augRDD);

        JavaRDD<Vector> clearedRDD = datasetRDD.map( line -> {
            String[] arr = line.split(";");
            double[] newArray = new double[2];
            newArray[0] = Double.parseDouble(arr[4]);
            newArray[1] = Double.parseDouble(arr[5]);

            return Vectors.dense(newArray);
        });

        clearedRDD.cache();
        // clearedRDD.take(10).forEach(out::println);

        // Cluster the data into two classes using KMeans
        KMeansModel clusters = KMeans.train(clearedRDD.rdd(), 8, 20);

        System.out.println("\nCluster centers:");
        for (Vector center: clusters.clusterCenters()) {
            System.out.println(" " + center);
        }

        double cost = clusters.computeCost(clearedRDD.rdd());
        System.out.println("Cost: " + cost);

        stopWatch.stop();
        out.println("Time is: " + stopWatch.getTime());

        jsc.close();
        System.clearProperty("spark.driver.port");

        /* Output:
        *   Cluster centers:
        *       [23.6057500041386,37.93163814902251]
        *       [0.0022237289726373542,0.005772217168435486]
        *       [0.5114804830917876,38.0770863285024]
        *       [7.663265606060599,48.3715108080808]
        *       [-86.77133916666651,37.9985275]
        *       [24.034467682291666,1.2636277864583334]
        *       [36.076945512820494,74.57123474358976]
        *       [26.952557001127374,39.08023142615557]
        *   Cost:
        *       262440.07448642654
        *   Time is:
        *       509800                                      */
    }
}