import os
import sys
import psycopg2
import psycopg2.extras
import pandas as pd
import pandas.io.sql as sqlio

print("Establishing connection.")
""" Connection to database """
try:
    conn = psycopg2.connect(
        host="localhost",
        database="gis_final_task",
        user="postgres",
        port="5432"
    )
    print("Connection Established.")
except Exception as error:
    print(str(error))
    sys.exit()

cur = conn.cursor()

print("Fetching all the distinct MMSIs.")
if not(os.path.exists("./Additional_Files/distinct_mmsis.csv")):
    query = "SELECT DISTINCT mmsi FROM main_dataset;"
    mmsis_df = sqlio.read_sql_query(query, conn)
    mmsis_df.to_csv("./Additional_Files/distinct_mmsis.csv", encoding="utf-8")
else:
    mmsis_df = pd.read_csv("./Additional_Files/distinct_mmsis.csv")
print("MMSIs ready.")
