import sys
import psycopg2
import psycopg2.extras
import pandas as pd
import pandas.io.sql as sqlio
from math import sin, cos, sqrt, atan2, radians

pd.options.mode.chained_assignment = None


def getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2):
    R = 6371  # Radius of the earth in km
    dLat = radians(lat2 - lat1)
    dLon = radians(lon2 - lon1)
    rLat1 = radians(lat1)
    rLat2 = radians(lat2)
    a = sin(dLat / 2) * sin(dLat / 2) + cos(rLat1) * cos(rLat2) * sin(dLon / 2) * sin(dLon / 2)
    c = 2 * atan2(sqrt(a), sqrt(1 - a))
    d = R * c  # Distance in km
    return d


def calc_velocity(dist_km, time_start, time_end):
    """Return 0 if time_start == time_end, avoid dividing by 0"""
    return dist_km / (time_end - time_start).seconds if time_end > time_start else 0


""" Connection to database """
try:
    conn = psycopg2.connect(
        host="localhost",
        database="gis_final_task",
        user="postgres",
        port="5432"
    )
    print("Connection Established")
except Exception as error:
    print(str(error))
    sys.exit()

cur = conn.cursor()

""""
    Calculating speed for all the points
    
    i)      get all the distinct mmsis
    ii)     for each mmsi order the data
    iii)    calculate speed from point to point 
    iv)     store it to db
    
    Note:
        step ii is done so for the data to be splitted into smaller
        bins and be easier to manage 
"""

query = "SELECT mmsi FROM trajectories;"
mmsis_df = sqlio.read_sql_query(query, conn)
for index, mmsi in mmsis_df.iterrows():
    try:
        print(mmsi[0])

        query = "SELECT mmsi, t, lat, lon FROM main_dataset WHERE mmsi = " + str(mmsi[0]) + ";"
        df = sqlio.read_sql_query(query, conn)

        " Add column with the date "
        df['date'] = pd.to_datetime(df['t'], unit='ms')

        try:
            " Group the sorted dataframe by ID, and grab the initial value for lat, lon, and time "
            df['lat0'] = df.groupby('mmsi')['lat'].transform(lambda x: x.iat[0])
            df['lon0'] = df.groupby('mmsi')['lon'].transform(lambda x: x.iat[0])
            df['date0'] = df.groupby('mmsi')['date'].transform(lambda x: x.iat[0])
        except Exception as error:
            print("Group by-s: " + str(error))
            continue

        " Sort the dataframe based on the timestamp "
        df = df.sort_values(by=['mmsi', 't'])

        " Create a new column for distance "
        try:
            df['dist_km'] = df.apply(
                lambda row: getDistanceFromLatLonInKm(
                    lat1=row['lat'],
                    lon1=row['lon'],
                    lat2=row['lat0'],
                    lon2=row['lon0']
                ),
                axis=1
            )
        except Exception as error:
            print("Distance: " + str(error))
            continue

        " Create a new column for velocity "
        try:
            df['velocity_knots'] = df.apply(
                lambda row: calc_velocity(
                    dist_km=row['dist_km'],
                    time_start=row['date0'],
                    time_end=row['date']
                ),
                axis=1
            )
        except Exception as error:
            print("Velocity: " + str(error))
            continue

        " Multiply by 3600 to convert second into hour "
        df.loc[:, 'velocity_knots'] *= 3600
        " Multiply by 0.539957 to convert kilometers to knots "
        df.loc[:, 'velocity_knots'] *= 0.54

        " Save every calculated speed into DB "
        for index2, row in df.iterrows():
            try:
                query = "UPDATE main_dataset SET speed = " + str(row['velocity_knots']) + " WHERE mmsi = " \
                        + str(mmsi[0]) + " AND t = " + str(row.loc['t']) + " AND lat = " + str(row.loc['lat']) \
                        + " AND lon = " + str(row.loc['lon']) + ";"
                cur.execute(query)
            except Exception as error:
                print("Deposition: " + str(error))
                continue
        conn.commit()
    except Exception as error:
        print("Outer: " + str(error))
        continue
conn.commit()
cur.close()
conn.close()
