import os
import sys
import psycopg2
import psycopg2.extras
import pandas as pd
import pandas.io.sql as sqlio
import geopandas as gpd
from shapely.geometry import LineString
from rdp import rdp

import preprocessing_functions as pf


pd.set_option('display.max_colwidth', -1)
pd.set_option('display.max_columns', None)

print("Establishing connection.")
""" Connection to database """
try:
    conn = psycopg2.connect(
        host="localhost",
        database="gis_final_task",
        user="postgres",
        port="5432"
    )
    print("Connection Established.")
except Exception as error:
    print(str(error))
    sys.exit()

cur = conn.cursor()

print("Fetching all the distinct MMSIs.")
if not(os.path.exists("./Additional_Files/distinct_mmsis.csv")):
    query = "SELECT DISTINCT mmsi FROM trajectories;"   # make for unique
    mmsis_df = sqlio.read_sql_query(query, conn)
    mmsis_df.to_csv("./Additional_Files/distinct_mmsis.csv", encoding="utf-8")
else:
    mmsis_df = pd.read_csv("./Additional_Files/distinct_mmsis.csv")
print("MMSIs ready.")

# Iterating within each unique mmsi
six_hours_in_milliseconds = 6*60*60*1000
for index, mmsi in mmsis_df.iterrows():
    print("Fetching data for MMSI: " + str(mmsi[1]) + "")
    query = "SELECT mmsi, t, lat, lon, geom FROM main_dataset WHERE mmsi = " + str(mmsi[1]) + ";"    # 256120000
    traj_gdf = gpd.GeoDataFrame.from_postgis(query, conn, geom_col="geom")
    print("Data ready.")

    print("Calculating speed and removing outliers.")
    try:
        traj_gdf = pf.calculate_velocity(traj_gdf)
    except:
        file = open('mistakes.txt', 'w+')
        file.write(str(mmsi[1]))
        continue
    # print(traj_gdf.columns)
    # -- Result: Index(['mmsi', 't', 'geom', 'velocity'], dtype='object')

    count_before = len(traj_gdf.index)
    # Delete records where speed exceeds 200 knots
    traj_gdf = traj_gdf[traj_gdf.velocity <= 200]
    """ If rows have been deleted, repeat the speed calculation
        and then delete the new values that exceeds 200 knots   """
    count_after = len(traj_gdf.index)
    if count_before > count_after:
        """ It means that records are deleted and
            so speed must be recalculated           """
        traj_gdf = pf.calculate_velocity(traj_gdf)
        # Delete records where speed exceeds 200 knots
        # traj_gdf = traj_gdf[traj_gdf.velocity <= 200]

    # Order by timestamp
    traj_gdf.sort_values(by=['t'])

    """ Split dataframe when timestamp gap is greater than 6h """
    # Get indexes from timestamp gaps that exceeds 6h
    print("Calculating time gaps.")
    gaps = []
    # If trajectory is more than 100 points, split due to software limitations
    counter = 0
    for temp_index, value in traj_gdf.iterrows():
        if temp_index == 0:
            continue

        counter += 1
        if counter >= 100:
            gaps.append(temp_index)
            counter = 0
            continue
        try:
            gap = traj_gdf.loc[temp_index, 't'] - traj_gdf.loc[temp_index - 1, 't']
            if gap > six_hours_in_milliseconds:
                if not temp_index == len(traj_gdf) - 1:
                    gaps.append(temp_index)
                    counter = 0
        except Exception as e:
            print(e)

    """ Applying Ramer-Douglas-Pecker algorithm to each trajectory
        for the reduction on the number of points                   """
    print("Saving...")
    if not gaps:
        # Means that list is empty, so I save all the points in one big trajectory
        print("one-big trajectory.")
        df_for_rdp = traj_gdf[['lon', 'lat']]
        reduced_array = rdp(df_for_rdp, epsilon=0.00001)
        print(str(len(df_for_rdp)) + " -> " + str(len(reduced_array)))

        # Convert list back to dataframe
        reduced_df = pd.DataFrame(reduced_array, columns=['lon', 'lat'])

        # Merge information from inital dataframe to the reduced one
        new_traj_gdf = pd.merge(reduced_df,
                                traj_gdf[['geom', 't', 'lon', 'lat', 'mmsi']],
                                on=['lon', 'lat'],
                                how='left')
        # Create the linestring of the trajectory
        temp_sr = new_traj_gdf.groupby(['mmsi'])['geom'].apply(lambda x: LineString(x.tolist()) if x.size > 1 else x.tolist())
        string_traj_parts = str(temp_sr.to_string()).split(' ')[4:]
        string_traj = " ".join(str(x) for x in string_traj_parts)
        query = "INSERT INTO trajectories(mmsi, traj) VALUES(" + str(mmsi[1]) + ", ST_GeomFromText('" + string_traj + "', 4326)) " + \
                "ON CONFLICT (mmsi, traj) DO NOTHING;"
        print("Query execution.")
        cur.execute(query)
        conn.commit()
        print("Commited!")
    else:
        print("many subtrajectories.")
        gap_mod = [0] + gaps + [max(gaps)+1]
        list_of_dfs = [traj_gdf.iloc[gap_mod[n]:gap_mod[n+1]] for n in range(len(gap_mod)-1)]
        for traje in list_of_dfs:
            df_for_rdp = traje[['lon', 'lat']]
            reduced_array = rdp(df_for_rdp, epsilon=0.00001)
            print(str(len(df_for_rdp)) + " -> " + str(len(reduced_array)))

            # Convert list back to dataframe
            reduced_df = pd.DataFrame(reduced_array, columns=['lon', 'lat'])

            # Merge information from inital dataframe to the reduced one
            traje = pd.merge(reduced_df,
                             traj_gdf[['geom', 't', 'lon', 'lat', 'mmsi']],
                             on=['lon', 'lat'],
                             how='left')

            # Ignore single points
            if len(traje) is 0:
                continue
            if len(traje) is 1:
                continue

            # Create the linestring of the trajectory
            temp_sr = traje.groupby(['mmsi'])['geom'].apply(lambda x: LineString(x.tolist()) if x.size > 1 else x.tolist())
            # Keep only the important part
            string_traj_parts = str(temp_sr.to_string()).split(' ')[4:]
            # Connect the splitted string parts
            string_traj = " ".join(str(x) for x in string_traj_parts)

            query = "INSERT INTO trajectories(mmsi, traj) VALUES(" + str(mmsi[1]) + ", ST_GeomFromText('" + string_traj + "', 4326)) " + \
                    "ON CONFLICT (mmsi, traj) DO NOTHING;"
            try:
                print("Query execution.")
                cur.execute(query)
                conn.commit()
                print("Commited!")
            except psycopg2.errors.ProgramLimitExceeded:
                conn.rollback()
                print("ProgramLimitExceeded error!")
            except psycopg2.errors.InFailedSqlTransaction:
                conn.rollback()
                print("InFailedSqlTransaction error!")
            except Exception as e:
                conn.rollback()
                print(e)

try:
    file.close()
except Exception as e:
    print(e)
cur.close()
conn.close()
