import os
import pandas as pd
from scipy.spatial.distance import euclidean
from fastdtw import fastdtw

# If trajectories similarity scores haven't calculated, do it now...
if not os.path.exists('Additional_Files/trajectories_scores.csv'):
    # Import trajectories from csv file into a dataframe
    df = pd.read_csv("./Additional_Files/traj_as_points.csv")
    print("File imported.")

    # Add column names
    df.columns = ['id', 'lat', 'lon', 'timestamp', 'mmsi']
    # Drop unnecessary columns
    df = df.drop(['id'], axis=1)
    # Remove milliseconds from timestamp
    df['timestamp'] = df['timestamp'].floordiv(1000)
    # Sort the values by mmsi and then by timestamp
    df.sort_values(['mmsi'])
    print("Data reconstructed.")

    # Get distinct mmsis and repeat it for each
    trajectories_list = []
    for distinct_mmsi in df.mmsi.unique():
        trajectories_list.append(df[df.mmsi == distinct_mmsi].drop(columns=['timestamp', 'mmsi']).to_numpy())
    print("List created.")

    # Initialize the length of the trajectories_list
    length = len(trajectories_list)

    # Create the score matrices
    print("Calculate Dynamic-Time-Warping score for each trajectories-set")
    scores = []
    for i in range(length):
        for j in range(length):
            if i == j:
                continue
            elif i < j:
                continue
            else:  # i > j
                distance, _ = fastdtw(trajectories_list[i], trajectories_list[j], dist=euclidean)
                scores.append([i, j, distance])

    scores_df = pd.DataFrame.from_records(scores)
    scores_df.columns = ['x', 'y', 'score']
    scores_df.to_csv('Additional_Files/trajectories_scores.csv', index=None, header=True)
else:
    scores_df = pd.read_csv('Additional_Files/trajectories_scores.csv')
print("Scores calculated.")
