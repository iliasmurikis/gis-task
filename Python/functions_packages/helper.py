import psycopg2
import numpy as np
import os
import configparser
import pandas as pd
from sklearn.cluster import DBSCAN, KMeans, OPTICS
from sklearn.neighbors import NearestCentroid
import geopandas as gpd
import contextily as ctx
from shapely.geometry import Point
import re
import matplotlib.pyplot as plt


def map_plot(df1, df2=None, title=None, fontsize=25, color=[None, None], figsize=(15, 15), attribution="", **kwargs):
    """
	Plot one or two dataframes on top of eachother.
	TODO - Add support for N Dataframes and more parameters, other that figsize.
	"""

    df1.crs = {'init': 'epsg:4326'}
    ax = df1.to_crs(epsg=3857).plot(figsize=figsize, color=color[0], **kwargs)
    if title is not None:
        ax.set_title(title, fontsize=fontsize)
    if df2 is not None:
        df2.crs = {'init': 'epsg:4326'}
        df2.to_crs(epsg=3857).plot(figsize=figsize, color=color[1], ax=ax, **kwargs)
    ctx.add_basemap(ax, attribution=attribution)
    ax.margins(0)
    ax.tick_params(left=False, labelleft=False, bottom=False, labelbottom=False)


def read_data_by_date(from_d='0510', till_d='0610'):
    properties = configparser.ConfigParser()
    properties.read(os.path.join('.', 'sql_server.ini'))
    properties = properties['SERVER']

    host = properties['host']
    db_name = properties['db_name']
    uname = properties['uname']
    pw = properties['pw']
    port = properties['port']

    #     traj_sql = f"SELECT * FROM ais_data.dynamic_ships_min_trip_card_3_segmented_12h_resampled_1min_v2 WHERE datetime>='2015-{re.findall('..',from_d)[1]}-{re.findall('..',from_d)[0]}' and datetime<'2015-{re.findall('..',till_d)[1]}-{re.findall('..',till_d)[0]}'"
    traj_sql = f"SELECT * FROM ais_data.dynamic_ships_cleaned limit 60000"

    con = psycopg2.connect(database=db_name, user=uname, password=pw, host=host, port=port)

    gdf = pd.read_sql_query(traj_sql, con=con)

    #     print(f'Read {len(gdf)} records from {gdf.datetime.min()} till {gdf.datetime.max()}')

    con.close()

    gdf['geom'] = np.nan
    gdf.geom = gdf[['lon', 'lat']].apply(lambda x: Point(x[0], x[1]), axis=1)
    return gpd.GeoDataFrame(gdf, geometry='geom')


def get_clusters_centers(X, labels, ignore=-1):
    clf = NearestCentroid()
    clf.fit(X[labels != ignore].values, labels[labels != ignore])
    return np.degrees(clf.centroids_)
