import pandas as pd
import matplotlib.pyplot as plt
import traja

# Import trajectories from csv file into a dataframe
df = pd.read_csv("./Additional_Files/traj_as_points.csv")
print("File imported.")

# Add column names
df.columns = ['id', 'lat', 'lon', 'timestamp', 'mmsi']
# Drop unnecessary columns
df = df.drop(['id'], axis=1)
# Remove milliseconds from timestamp
df['timestamp'] = df['timestamp'].floordiv(1000)
# Sort the values by mmsi and then by timestamp
df.sort_values(['mmsi'])
# Rename dataframe for traja resampling
df.columns = ['y', 'x', 'time', 'mmsi']
print("Data reconstructed.")

# Get distinct mmsis and repeat it for each
for distinct_mmsi in df.mmsi.unique():
    resampled = traja.resample_time(df[df.mmsi == distinct_mmsi], '30S')  # 30 seconds
    resampled.traja.plot()
    plt.show()
