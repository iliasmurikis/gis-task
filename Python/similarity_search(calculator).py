import pandas as pd

# Import scores csv
df = pd.read_csv("./Additional_Files/trajectories_scores.csv")

# Sort by score
scores_df = df.sort_values(by='score', ascending=False)
print(scores_df.head(15).to_string(index=False))
