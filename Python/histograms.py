import os
import sys
import psycopg2
import psycopg2.extras
import pandas as pd
import pandas.io.sql as sqlio
import matplotlib.pyplot as plt
import seaborn as sns

pd.options.mode.chained_assignment = None

""" Connection to database """
try:
    conn = psycopg2.connect(
        host="localhost",
        database="gis_final_task",
        user="postgres",
        port="5432"
    )
    print("Connection Established. \n")
except Exception as error:
    print(str(error))
    sys.exit()

cur = conn.cursor()

# Use True for 3.2.2 and False for 3.2.3
if True:
    """ 3.2.2 Create histogram for the data distribution
        in the two month period                             """

    # If csv with means don't exist, create it...
    if not os.path.exists("./Additional_Files/means.csv"):
        mmsis_list = []
        means_list = []

        distinct_mmsis_df = pd.read_csv("./Additional_Files/distinct_mmsis.csv")
        distict_mmsis_list = distinct_mmsis_df['mmsi'].to_list()
        for i, mmsi in enumerate(distict_mmsis_list):
            query = "SELECT mmsi, t FROM main_dataset WHERE mmsi = " + str(mmsi) + ";"
            df = sqlio.read_sql_query(query, conn)
            if len(df.index) <= 1:
                continue

            # Sort by mmsi and timestamp
            df = df.sort_values(by=['mmsi', 't'])
            # Divide timestamp column by 1000
            df['t'] = df['t'].div(1000)

            # Create the dataframe of differences
            differences = df.groupby(by='mmsi').diff(axis=0)
            differences.columns = ['difference']
            complete = pd.concat([df, differences], ignore_index=True, axis=1)
            complete.columns = ['mmsi', 'timestamp', 'differences']

            # Calculate the mean of differences column
            mean = int(round(complete['differences'].mean()))
            means_list.append(mean)
            mmsis_list.append(mmsi)
            print(i, mmsi, mean)

        means_df = pd.DataFrame(list(zip(means_list, mmsis_list)))
        means_df.columns = ["mean", "mmsi"]
    else:
        means_df = pd.read_csv("./Additional_Files/means.csv")

    smallest_half_df = means_df.nsmallest(10, 'mean')
    greatest_half_df = means_df.nlargest(10, 'mean')
    smallest_half_df = smallest_half_df.sort_values(by=['mean'])
    greatest_half_df = greatest_half_df.sort_values(by=['mean'])

    # Plotting...
    plt.figure(figsize=(16, 9))
    fig, axs = plt.subplots(ncols=2)

    plot1 = sns.barplot(x="mmsi", y="mean", data=smallest_half_df, order=smallest_half_df['mmsi'],
                        orient="v", color="salmon", ci=None, ax=axs[0])
    for item in plot1.get_xticklabels():
        item.set_rotation(90)

    plot2 = sns.barplot(x="mmsi", y="mean", data=greatest_half_df, order=greatest_half_df['mmsi'],
                        orient="v", color="salmon", ci=None, ax=axs[1])
    for item in plot2.get_xticklabels():
        item.set_rotation(90)

    plt.show()
else:
    """ 3.2.3 Create histogram for the data distribution
        in the two month period                             """

    if not(os.path.exists("./Additional_Files/speeds.csv")):
        query = "SELECT speed::INTEGER FROM main_dataset WHERE speed IS NOT NULL AND speed <= 200 AND speed > 0;"
        df = sqlio.read_sql_query(query, conn)
        df.to_csv("./Additonal_Files/speeds.csv", encoding="utf-8")

    df = pd.read_csv("./Additional_Files/speeds.csv")
    print("Dataframe ready. \n")

    if not(os.path.exists("./Additional_Files/frequencies.csv")):
        bins = pd.cut(x=df["speed"], bins=[0, 2, 4, 8, 16, 32, 64, 128, 200])
        to_be_written = df.groupby(bins)['speed'].agg(['count'])
        to_be_written.to_csv("./Additional_Files/frequencies.csv", encoding="utf-8")

    df = pd.read_csv("./Additional_Files/frequencies.csv")
    print("Frequencies calculated. \n")

    df.columns = ['Speed', 'Occurrences']

    sns.barplot(x="Speed", y="Occurrences", data=df, orient="v", color="salmon", ci=None)
    plt.title("Speeds")
    plt.show()
