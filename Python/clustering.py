import sys
import time
import psycopg2
import psycopg2.extras
import pandas.io.sql as sqlio
import numpy as np
from sklearn.cluster import DBSCAN
from sklearn.cluster import KMeans
from sklearn.cluster import OPTICS
import matplotlib.pyplot as plt

import helper

start = time.time()
""" Connection to database """
try:
    conn = psycopg2.connect(
        host="localhost",
        database="gis_final_task",
        user="postgres",
        port="5432"
    )
    print("Connection Established. \n")
except Exception as error:
    print(str(error))
    sys.exit()

cur = conn.cursor()

query = "SELECT lat, lon FROM main_dataset LIMIT 1000000;"
df = sqlio.read_sql_query(query, conn)
X = df.to_numpy()
print("Data ready. \n")

"""
    Clustering options:
        ~ DBSCAN  = dbscan
        ~ OPTICS  = optics
        ~ K-Means = kmeans
"""
option = "kmeans"
if option == "dbscan":  # 30000 used
    print("Applying DBSCAN:")
    X = np.radians(df[['lat', 'lon']])
    db = DBSCAN(eps=1/6371., min_samples=len(df)//50, algorithm='ball_tree', metric='haversine').fit(X)
    set(db.labels_)
    # print(helper.get_clusters_centers(X, db.labels_))
    print("Plotting: \n")
    Xnew = df.to_numpy()
    plt.scatter(Xnew[:, 0], Xnew[:, 1])
    plt.scatter(helper.get_clusters_centers(X, db.labels_)[:, 0], helper.get_clusters_centers(X, db.labels_)[:, 1], s=200, c='red')
    plt.show()
elif option == "optics":    # 30000 used
    print("Applying optics:")
    X = np.radians(df[['lat', 'lon']])
    optics = OPTICS(max_eps=1/6371, min_samples=len(df)//50, metric='haversine').fit(X)
    set(optics.labels_)
    print("Plotting: \n")
    Xnew = df.to_numpy()
    plt.scatter(Xnew[:, 0], Xnew[:, 1])
    plt.scatter(helper.get_clusters_centers(X, optics.labels_)[:, 0], helper.get_clusters_centers(X, optics.labels_)[:, 1], s=200, c='red')
    plt.show()
elif option == "kmeans":    # 1.000.000 used
    k = 8
    if k is None:
        print("Applying Elbow method for the determination of K:")
        wcss = []
        for i in range(1, 11):
            kmeans = KMeans(n_clusters=i, init='k-means++', max_iter=300, n_init=10, random_state=0)
            kmeans.fit(X)
            wcss.append(kmeans.inertia_)
        plt.plot(range(1, 11), wcss)
        plt.title('Elbow Method')
        plt.xlabel('Number of clusters')
        plt.ylabel('WCSS')
        plt.show()
    else:
        print("Executing clustering with K-Means:")
        kmeans = KMeans(n_clusters=k, init='k-means++', max_iter=300, n_init=10, random_state=0)
        pred_y = kmeans.fit_predict(X)

        finish = time.time()
        total_time = finish - start
        print("Time is: " + str(total_time))
        # Total time: 57.006641149520874 (float in seconds???)

        plt.scatter(X[:, 0], X[:, 1])
        plt.scatter(kmeans.cluster_centers_[:, 0], kmeans.cluster_centers_[:, 1], s=200, c='red')
        plt.show()
else:
    print("Unknown algorithm!")


