import pandas as pd
from EvolvingClusters.EvolvingClusters import evolving_clusters

# Import trajectories from csv file into a dataframe
df = pd.read_csv("./Additional_Files/traj_as_points.csv")
print("File imported.")
# Add column names
df.columns = ['id', 'lat', 'lon', 'datetime', 'mmsi']

# Drop almost half of the records
# df = df.head(15000000)

# Remove milliseconds from timestamp
df['datetime'] = df['datetime'].floordiv(1000)
# Convert timestamp to datetime
df['datetime'] = pd.to_datetime(df['datetime'], unit='s')
print("Preprocessing completed.")

# Apply Evolving Clustering on these trajectories
print("Applying Evolving Clusters algorithm.")
res = evolving_clusters(df, 'convoys', min_cardinality=3, time_threshold=10, distance_threshold=10000, disable_progress_bar=False)
print("Clusteing completed.")
print(res.head())
res.to_csv('convoys_output.csv')
print("Output completed.")
